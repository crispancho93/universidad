﻿using System;

namespace Models
{
    public class Cliente
    {
        public int IdCliente { get; set; }
        public string StrNombre { get; set; }
        public string NumDocumento { get; set; }
        public string StrDireccion { get; set; }
        public string StrTelefono { get; set; }
        public string StrEmail { get; set; }
        public DateTime DtmFechaModifica { get; set; }
        public string StrUsuarioModifica { get; set; }
    }
}
