﻿using System;

namespace Models
{
    public class Empleado
    {
        public int IdEmpleado { get; set; }
        public string StrNombre { get; set; }
        public string NumDocumento { get; set; }
        public string StrDireccion { get; set; }
        public string StrTelefono { get; set; }
        public string StrEmail { get; set; }
        public int IdRolEmpleado { get; set; }
        public string DescripcionRol { get; set; }
        public DateTime DtmIngreso { get; set; }
        public DateTime DtmRetiro { get; set; }
        public string StrDatosAdicionales { get; set; }
        public DateTime DtmFechaModifica { get; set; }
        public string StrUsuarioModifico { get; set; }
    }
}
