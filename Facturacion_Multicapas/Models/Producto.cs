﻿using System;

namespace Models
{
    public class Producto
    {
        public int IdProducto { get; set; }
        public string StrNombre { get; set; }
        public string StrCodigo { get; set; }
        public string NumPrecioCompra { get; set; }
        public string NumPrecioVenta { get; set; }
        public int IdCategoria { get; set; }
        public string DescripcionCategoria { get; set; }
        public string StrDetalle { get; set; }
        public string StrFoto { get; set; }
        public int NumStock { get; set; }
        public DateTime DtmFechaModifica { get; set; }
        public string StrUsuarioModifica { get; set; }
    }
}
