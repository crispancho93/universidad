﻿namespace Models
{
    public class Rol
    {
        public int IdRolEmpleado { get; set; }
        public string StrDescripcion { get; set; }
    }
}
