﻿using System;

namespace Models
{
    public class Categoria
    {
        public int IdCategoria { get; set; }
        public string StrDescripcion { get; set; }
        public DateTime DtmFechaModifica { get; set; }
        public string StrUsuarioModifico { get; set; }
    }
}
