﻿using System;

namespace Models
{
    public class Seguridad
    {
        public int IdSeguridad { get; set; }
        public int IdEmpleado { get; set; }
        public string StrUsuario { get; set; }
        public string StrClave { get; set; }
        public DateTime DtmFechaModifica { get; set; }
        public string StrUsuarioModifico { get; set; }
    }
}
