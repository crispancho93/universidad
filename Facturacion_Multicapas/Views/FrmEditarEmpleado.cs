﻿using Models;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmEditarEmpleado : Form
    {
        private readonly EmpleadoServ _empleadoServ;
        private readonly RolServ _rolServ;
        public int IdEmpleado { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmEditarEmpleado()
        {
            InitializeComponent();
            _empleadoServ = new EmpleadoServ();
            _rolServ = new RolServ();
        }

        private void FrmEditarEmpleado_Load(object sender, EventArgs e)
        {
            var roles = _rolServ.GetAll();
            cmbRol.DataSource = roles;
            cmbRol.DisplayMember = "StrDescripcion";
            cmbRol.ValueMember = "IdRolEmpleado";

            if (IdEmpleado == 0)
            {
                lbTitulo.Text = "NUEVO EMPLEADO";
            }
            else
            {
                var empleado = _empleadoServ.GetById(IdEmpleado);
                lbTitulo.Text = "EDITAR EMPLEADO";
                txtNombre.Text = empleado.StrNombre;
                txtDocumento.Text = empleado.NumDocumento;
                txtDireccion.Text = empleado.StrDireccion;
                txtTelefono.Text = empleado.StrTelefono;
                txtEmail.Text = empleado.StrEmail;
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != string.Empty && txtDocumento.Text != string.Empty)
            {
                var empleado = new Empleado
                {
                    IdEmpleado = IdEmpleado,
                    StrNombre = txtNombre.Text,
                    NumDocumento = txtDocumento.Text,
                    StrDireccion = txtDireccion.Text,
                    StrEmail = txtEmail.Text,
                    IdRolEmpleado = (int)cmbRol.SelectedValue,
                    DtmIngreso = Convert.ToDateTime(txtFechaIngreso.Text),
                    DtmRetiro = Convert.ToDateTime(txtFechaRetiro.Text),
                    StrDatosAdicionales = txtDatosAdicionales.Text,
                    StrUsuarioModifico = "Crispancho",
                    DtmFechaModifica = DateTime.Now,
                };
                var affected = _empleadoServ.Save(empleado);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
