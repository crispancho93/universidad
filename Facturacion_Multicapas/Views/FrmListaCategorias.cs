﻿using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmListaCategorias : Form
    {
        private readonly CategoriaServ _categoriaServ;

        public FrmListaCategorias()
        {
            InitializeComponent();
            _categoriaServ = new CategoriaServ();
        }

        private void FrmListaCategorias_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvCategorias.Rows.Clear();
            var data = _categoriaServ.GetAll();

            foreach (var row in data)
            {
                dgvCategorias.Rows.Add(
                    row.IdCategoria,
                    row.StrDescripcion
                );
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCliente = new FrmEditarCategoria
            {
                FrmParent = this
            };
            frmCliente.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvCategorias.CurrentRow.Index;

            if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCliente = new FrmEditarCategoria
                {
                    IdCategoria = int.Parse(dgvCategorias[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCliente.ShowDialog();
            }
            else if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    _categoriaServ.Delete(int.Parse(dgvCategorias[0, index].Value.ToString()));
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvCategorias[0, index].Value.ToString()}");
                    LoadData();
                }
            }
        }
    }
}
