﻿using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmListaEmpleados : Form
    {

        private readonly EmpleadoServ _empleadoServ;

        public FrmListaEmpleados()
        {
            InitializeComponent();
            _empleadoServ = new EmpleadoServ();
        }

        private void FrmListaEmpleados_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvEmpleados.Rows.Clear();
            var data = _empleadoServ.GetAll();

            foreach (var empleado in data)
            {
                dgvEmpleados.Rows.Add (
                    empleado.IdEmpleado, 
                    empleado.StrNombre, 
                    empleado.NumDocumento, 
                    empleado.StrEmail, 
                    empleado.StrTelefono,
                    empleado.StrDireccion,
                    empleado.DtmIngreso,
                    empleado.DtmRetiro,
                    empleado.DescripcionRol,
                    empleado.StrDatosAdicionales
                );
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FrmEditarEmpleado
            {
                FrmParent = this
            };
            frm.ShowDialog();
        }

        private void dgvEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvEmpleados.CurrentRow.Index;

            if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCliente = new FrmEditarEmpleado
                {
                    IdEmpleado = int.Parse(dgvEmpleados[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCliente.ShowDialog();
            } else if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    _empleadoServ.Delete(int.Parse(dgvEmpleados[0, index].Value.ToString()));
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvEmpleados[0, index].Value.ToString()}");
                    LoadData();
                }
            }

        }
    }
}
