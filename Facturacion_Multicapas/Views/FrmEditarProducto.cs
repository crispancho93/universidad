﻿using Models;
using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmEditarProducto : Form
    {
        public int IdProducto { get; set; }
        public dynamic FrmParent { get; set; }
        private readonly ProductoServ _productoServ;
        private readonly CategoriaServ _categoriaServ;

        public FrmEditarProducto()
        {
            InitializeComponent();
            _productoServ = new ProductoServ();
            _categoriaServ = new CategoriaServ();
        }

        private void FrmEditarProducto_Load(object sender, EventArgs e)
        {
            var categorias = _categoriaServ.GetAll();
            cmbCategoria.DataSource = categorias;
            cmbCategoria.DisplayMember = "StrDescripcion";
            cmbCategoria.ValueMember = "IdCategoria";

            if (IdProducto == 0)
            {
                lbTitulo.Text = "NUEVO PRODUCTO";
            }
            else
            {
                var producto = _productoServ.GetById(IdProducto);
                lbTitulo.Text = "EDITAR PRODUCTO";
                txtNombre.Text = producto.StrNombre;
                txtReferencia.Text = producto.StrCodigo;
                txtPrecioCompra.Text = producto.NumPrecioCompra;
                txtPrecioVenta.Text = producto.NumPrecioVenta;
                txtStock.Text = producto.NumStock.ToString();
                txtImagen.Text = producto.StrFoto;
                txtDetalles.Text = producto.StrDetalle;
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != string.Empty)
            {
                var producto = new Producto
                {
                    IdProducto = IdProducto,
                    IdCategoria = (int)cmbCategoria.SelectedValue,
                    StrNombre = txtNombre.Text,
                    StrCodigo = txtReferencia.Text,
                    NumPrecioCompra = txtPrecioCompra.Text,
                    NumPrecioVenta = txtPrecioVenta.Text,
                    NumStock = int.Parse(txtStock.Text),
                    StrFoto = txtImagen.Text,
                    StrDetalle = txtDetalles.Text,
                    StrUsuarioModifica = "Crispancho",
                    DtmFechaModifica = DateTime.Now,
                };
                var affected = _productoServ.Save(producto);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
