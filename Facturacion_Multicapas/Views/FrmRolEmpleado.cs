﻿using Models;
using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmRolEmpleado : Form
    {
        private readonly RolServ _rolServ;
        public int IdRolEmpleado { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmRolEmpleado()
        {
            InitializeComponent();
            _rolServ = new RolServ();
        }

        private void FrmRolEmpleado_Load(object sender, EventArgs e)
        {
            if (IdRolEmpleado == 0)
            {
                lbTitulo.Text = "NUEVO ROL EMPLEADO";
            }
            else
            {
                var rol = _rolServ.GetById(IdRolEmpleado);
                lbTitulo.Text = "EDITAR ROL EMPLEADO";
                txtNombre.Text = rol.StrDescripcion;
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(txtNombre.Text != string.Empty)
            {
                var rol = new Rol
                {
                    IdRolEmpleado = IdRolEmpleado,
                    StrDescripcion = txtNombre.Text
                };
                var affected = _rolServ.Save(rol);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
