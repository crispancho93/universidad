﻿using Models;
using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmEditarCliente : Form
    {
        public int IdCliente { get; set; }
        public dynamic FrmParent { get; set; }
        private readonly ClienteServ _clienteServ;

        public FrmEditarCliente()
        {
            InitializeComponent();
            _clienteServ = new ClienteServ();
        }

        private void FrmEditarCliente_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO CLIENTE";
            }
            else
            {
                var cliente = _clienteServ.GetById(IdCliente);
                lbTitulo.Text = "EDITAR CLIENTE";
                txtNombre.Text = cliente.StrNombre;
                txtDocumento.Text = cliente.NumDocumento;
                txtDireccion.Text = cliente.StrDireccion;
                txtTelefono.Text = cliente.StrTelefono;
                txtEmail.Text = cliente.StrEmail;
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != string.Empty && txtDocumento.Text != string.Empty)
            {
                var cliente = new Cliente
                {
                    IdCliente = IdCliente,
                    StrNombre = txtNombre.Text,
                    NumDocumento = txtDocumento.Text,
                    StrDireccion = txtDireccion.Text,
                    StrTelefono = txtTelefono.Text,
                    StrEmail = txtEmail.Text,
                    StrUsuarioModifica = "Crispancho",
                    DtmFechaModifica = DateTime.Now,
                };
                var affected = _clienteServ.Save(cliente);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
