﻿using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmListaProductos : Form
    {
        private readonly ProductoServ _productoServ;

        public FrmListaProductos()
        {
            InitializeComponent();
            _productoServ = new ProductoServ();
        }

        private void FrmListaProductos_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvProductos.Rows.Clear();
            var data = _productoServ.GetAll();

            foreach (var producto in data)
            {
                dgvProductos.Rows.Add(
                    producto.IdProducto,
                    producto.StrNombre,
                    producto.StrCodigo,
                    producto.DescripcionCategoria,
                    producto.NumPrecioCompra,
                    producto.NumPrecioVenta,
                    producto.NumStock,
                    producto.StrFoto,
                    producto.StrDetalle
                );
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FrmEditarProducto
            {
                FrmParent = this
            };
            frm.ShowDialog();

        }

        private void dgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvProductos.CurrentRow.Index;

            if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCliente = new FrmEditarProducto
                {
                    IdProducto = int.Parse(dgvProductos[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCliente.ShowDialog();
            }
            else if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    _productoServ.Delete(int.Parse(dgvProductos[0, index].Value.ToString()));
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvProductos[0, index].Value.ToString()}");
                    LoadData();
                }
            }
        }
    }
}
