﻿using Models;
using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmEditarCategoria : Form
    {
        public int IdCategoria { get; set; }
        public dynamic FrmParent { get; set; }
        private readonly CategoriaServ _categoriaServ;

        public FrmEditarCategoria()
        {
            InitializeComponent();
            _categoriaServ = new CategoriaServ();
        }

        private void FrmEditarCategoria_Load(object sender, EventArgs e)
        {
            if (IdCategoria == 0)
            {
                lbTitulo.Text = "NUEVO CATEGORIA";
            }
            else
            {
                var categoria = _categoriaServ.GetById(IdCategoria);
                lbTitulo.Text = "EDITAR CATEGORIA";
                txtNombre.Text = categoria.StrDescripcion;
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != string.Empty)
            {
                var categoria = new Categoria
                {
                    IdCategoria = IdCategoria,
                    StrDescripcion = txtNombre.Text,
                    StrUsuarioModifico = "Crispancho",
                    DtmFechaModifica = DateTime.Now,
                };
                var affected = _categoriaServ.Save(categoria);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
