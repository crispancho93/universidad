﻿using Models;
using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmLogin : Form
    {
        private readonly UserServ _userServ;

        public FrmLogin()
        {
            InitializeComponent();
            _userServ = new UserServ();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            txtUsuario.Text = "pepe";
            txtClave.Text = "123";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != string.Empty && txtClave.Text != string.Empty)
            {
                var user = new User
                {
                    StrUsuario = txtUsuario.Text,
                    StrClave = txtClave.Text,
                };
                if (_userServ.Login(user))
                {
                    MessageBox.Show($"Bienvenido {user.StrUsuario.ToUpper()}");
                    var frmPpal = new FrmPrincipal();
                    Hide();
                    frmPpal.Show();
                }
                else
                {
                    MessageBox.Show("Usuario o clave incorrectos, intenta de nuevo.");
                }
            }
            else
            {
                MessageBox.Show("El usuario y la clave son obligatorios.");
            }
        }
    }
}
