﻿using Models;
using Services;
using System;
using System.Linq;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmAdminSeguridad : Form
    {
        private readonly EmpleadoServ _empleadoServ;
        private readonly SeguridadServ _seguridadServ;

        public FrmAdminSeguridad()
        {
            InitializeComponent();
            _empleadoServ = new EmpleadoServ();
            _seguridadServ = new SeguridadServ();
        }

        private void FrmAdminSeguridad_Load(object sender, EventArgs e)
        {
            // LLeno combo empleados
            var empleados = _empleadoServ.GetAll();
            cmbEmpleado.DataSource = empleados;
            cmbEmpleado.DisplayMember = "strNombre";
            cmbEmpleado.ValueMember = "IdEmpleado";

            // LLeno campos de seguridad
            var first = empleados.FirstOrDefault();
            if (first != null)
            {
                var security = _seguridadServ.GetByEmploye(first.IdEmpleado);
                if (security != null)
                {
                    txtUsuario.Text = security.StrUsuario;
                    txtClave.Text = security.StrClave;
                }
                else
                {
                    txtUsuario.Text = "";
                    txtClave.Text = "";
                }
            }
        }

        private void cmbEmpleado_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (!cb.Focused)
            {
                return;
            }

            var security = _seguridadServ.GetByEmploye(int.Parse(cmbEmpleado.SelectedValue.ToString()));
            if (security != null)
            {
                txtUsuario.Text = security.StrUsuario;
                txtClave.Text = security.StrClave;
            }
            else
            {
                txtUsuario.Text = "";
                txtClave.Text = "";
            }
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (txtUsuario.Text != string.Empty && txtClave.Text != string.Empty && int.Parse(cmbEmpleado.SelectedValue.ToString()) > 0)
            {
                var security = new Seguridad
                {
                    IdEmpleado = int.Parse(cmbEmpleado.SelectedValue.ToString()),
                    StrUsuario = txtUsuario.Text,
                    StrClave = txtClave.Text,
                    DtmFechaModifica = DateTime.Now,
                    StrUsuarioModifico = "Crispancho"
                };

                var affected = _seguridadServ.Save(security);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
