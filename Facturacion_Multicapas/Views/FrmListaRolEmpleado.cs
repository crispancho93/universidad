﻿using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmListaRolEmpleado : Form
    {
        private readonly RolServ _rolServ;

        public FrmListaRolEmpleado()
        {
            InitializeComponent();
            _rolServ = new RolServ();
        }

        private void FrmListaRolEmpleado_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvRolEmpleado.Rows.Clear();
            var data = _rolServ.GetAll();

            foreach(var rol in data)
            {
                dgvRolEmpleado.Rows.Add(rol.IdRolEmpleado, rol.StrDescripcion);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmRolEmpleado = new FrmRolEmpleado
            {
                FrmParent = this,
            };
            frmRolEmpleado.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvRolEmpleado.CurrentRow.Index;

            if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmRolEmpleado = new FrmRolEmpleado
                {
                    IdRolEmpleado = int.Parse(dgvRolEmpleado[0, index].Value.ToString()),
                    FrmParent = this,
                };
                frmRolEmpleado.ShowDialog();
            } else if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    _rolServ.Delete(int.Parse(dgvRolEmpleado[0, index].Value.ToString()));
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvRolEmpleado[0, index].Value.ToString()}");
                    LoadData();
                }
            }

        }
    }
}
