﻿using Services;
using System;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmListaClientes : Form
    {
        private readonly ClienteServ _clienteServ;

        public FrmListaClientes()
        {
            InitializeComponent();
            _clienteServ = new ClienteServ();
        }

        private void FrmListaClientes_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvClientes.Rows.Clear();
            var data = _clienteServ.GetAll();

            foreach (var cliente in data)
            {
                dgvClientes.Rows.Add(
                    cliente.IdCliente,
                    cliente.StrNombre,
                    cliente.NumDocumento,
                    cliente.StrEmail,
                    cliente.StrTelefono
                );
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCliente = new FrmEditarCliente
            {
                FrmParent = this
            };
            frmCliente.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvClientes.CurrentRow.Index;

            if (dgvClientes.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCliente = new FrmEditarCliente
                {
                    IdCliente = int.Parse(dgvClientes[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCliente.ShowDialog();
            }
            else if (dgvClientes.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    _clienteServ.Delete(int.Parse(dgvClientes[0, index].Value.ToString()));
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvClientes[0, index].Value.ToString()}");
                    LoadData();
                }
            }
        }
    }
}
