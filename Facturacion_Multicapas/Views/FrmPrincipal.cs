﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace Views
{
    public partial class FrmPrincipal : MaterialForm
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmListaClientes());
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmListaProductos());
        }

        private void btnCategorias_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmListaCategorias());
        }

        private void btnRoles_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmListaRolEmpleado());
        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmListaEmpleados());
        }

        private void btnSeguridad_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmAdminSeguridad());
        }

        private void bntAyuda_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmAyuda());
        }

        private void btnAcercaDe_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmAcercaDe());
        }

        private void btnInformes_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmInformes());
        }

        private void btnFacturas_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmFacturas());
        }

        private void OpenForm(Form form)
        {
            if (pnlContenedor.Controls.Count > 0)
            {
                pnlContenedor.Controls.RemoveAt(0);
            }
            form.TopLevel = false;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Dock = DockStyle.Fill;
            pnlContenedor.Controls.Add(form);
            form.Show();
        }
    }
}
