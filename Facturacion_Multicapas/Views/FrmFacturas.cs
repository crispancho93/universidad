﻿using Services;
using System;
using System.Data;
using System.Windows.Forms;

namespace Views
{
    public partial class FrmFacturas : Form
    {
        private readonly FacturaServ _facturaServ;
        private readonly ClienteServ _clienteServ;

        public FrmFacturas()
        {
            InitializeComponent();
            _facturaServ = new FacturaServ();
            _clienteServ = new ClienteServ();
        }

        private void FrmFacturas_Load(object sender, EventArgs e)
        {
            var roles = _clienteServ.GetAll();
            cmbClientes.DataSource = roles;
            cmbClientes.DisplayMember = "StrNombre";
            cmbClientes.ValueMember = "IdCliente";

            var data = _facturaServ.GetAll();
            foreach (DataRow row in data.Rows)
            {
                dgvFacturas.Rows.Add(
                       row["IdFactura"],
                       row["StrNombreCliente"],
                       row["StrNombreEmpleado"],
                       row["DtmFechaModifica"],
                       row["NumValorTotal"],
                       row["NumImpuesto"],
                       row["IdEstado"]
                   );
            }
        }
    }
}
