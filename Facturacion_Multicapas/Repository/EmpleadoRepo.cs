﻿using Dapper;
using Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public  class EmpleadoRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Save(Empleado user)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    user.IdEmpleado,
                    user.StrNombre,
                    user.NumDocumento,
                    user.StrDireccion,
                    user.StrTelefono,
                    user.StrEmail,
                    user.IdRolEmpleado,
                    user.DtmIngreso,
                    user.DtmRetiro,
                    user.StrDatosAdicionales,
                    user.DtmFechaModifica,
                    user.StrUsuarioModifico
                };
                return conn.Execute("actualizar_Empleado", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Empleado> GetAll()
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     e.[IdEmpleado]
                    ,e.[StrNombre]
                    ,e.[NumDocumento]
                    ,e.[StrDireccion]
                    ,e.[StrTelefono]
                    ,e.[StrEmail]
                    ,e.[IdRolEmpleado]
                    ,r.[StrDescripcion] DescripcionRol
                    ,e.[DtmIngreso]
                    ,e.[DtmRetiro]
                    ,e.[StrDatosAdicionales]
                    ,e.[DtmFechaModifica]
                    ,e.[StrUsuarioModifico]
                    ,e.[strDatosAdicionales]
                FROM [dbo].[TBLEMPLEADO] e
                INNER JOIN [TBLROLES] r ON r.IdRolEmpleado = e.IdRolEmpleado";
                return conn.Query<Empleado>(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public Empleado GetById(int IdEmpleado)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdEmpleado]
                    ,[StrNombre]
                    ,[NumDocumento]
                    ,[StrDireccion]
                    ,[StrTelefono]
                    ,[StrEmail]
                    ,[IdRolEmpleado]
                    ,[DtmIngreso]
                    ,[DtmRetiro]
                    ,[StrDatosAdicionales]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifico]
                FROM [dbo].[TBLEMPLEADO] WHERE IdEmpleado = @IdEmpleado";
                return conn.Query<Empleado>(query, new { IdEmpleado }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public int Delete(int IdEmpleado)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                return conn.Execute("DELETE FROM TBLEMPLEADO WHERE IdEmpleado = @IdEmpleado", new { IdEmpleado });
            }
        }
    }
}
