﻿using Dapper;
using Models;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Repository
{
    public class SeguridadRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="security"></param>
        /// <returns></returns>
        public int Save(Seguridad security)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    security.IdEmpleado,
                    security.StrUsuario,
                    security.StrClave,
                    security.DtmFechaModifica,
                    security.StrUsuarioModifico,
                };
                return conn.Execute("actualizar_Seguridad", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public Seguridad GetByEmploye(int IdEmpleado)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [StrUsuario]
                    ,[StrClave]
                FROM [dbo].[TBLSEGURIDAD] WHERE IdEmpleado = @IdEmpleado";
                return conn.Query<Seguridad>(query, new { IdEmpleado }).FirstOrDefault();
            }
        }
    }
}
