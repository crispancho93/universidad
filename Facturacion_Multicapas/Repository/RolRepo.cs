﻿using Dapper;
using Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Repository
{
    public  class RolRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        public int Save(Rol rol)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    rol.IdRolEmpleado,
                    rol.StrDescripcion,
                };
                return conn.Execute("actualizar_RolEmpleado", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Rol> GetAll()
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdRolEmpleado]
                    ,[StrDescripcion]
                FROM [dbo].[TBLROLES]";
                return conn.Query<Rol>(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdRolEmpleado"></param>
        /// <returns></returns>
        public Rol GetById(int IdRolEmpleado)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdRolEmpleado]
                    ,[StrDescripcion]
                FROM [dbo].[TBLROLES] WHERE IdRolEmpleado = @IdRolEmpleado";
                return conn.Query<Rol>(query, new { IdRolEmpleado }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdRolEmpleado"></param>
        /// <returns></returns>
        public int Delete(int IdRolEmpleado)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                return conn.Execute("DELETE FROM [TBLROLES] WHERE IdRolEmpleado = @IdRolEmpleado", new { IdRolEmpleado });
            }
        }
    }
}
