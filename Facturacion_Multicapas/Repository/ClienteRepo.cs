﻿using Dapper;
using Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class ClienteRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cliente"></param>
        /// <returns></returns>
        public int Save(Cliente cliente)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    cliente.IdCliente,
                    cliente.StrNombre,
                    cliente.NumDocumento,
                    cliente.StrDireccion,
                    cliente.StrTelefono,
                    cliente.StrEmail,
                    cliente.DtmFechaModifica,
                    cliente.StrUsuarioModifica
                };
                return conn.Execute("actualizar_Cliente", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Cliente> GetAll()
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdCliente]
                    ,[StrNombre]
                    ,[NumDocumento]
                    ,[StrDireccion]
                    ,[StrTelefono]
                    ,[StrEmail]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifica]
                FROM [dbo].[TBLCLIENTES]";
                return conn.Query<Cliente>(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCliente"></param>
        /// <returns></returns>
        public Cliente GetById(int IdCliente)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdCliente]
                    ,[StrNombre]
                    ,[NumDocumento]
                    ,[StrDireccion]
                    ,[StrTelefono]
                    ,[StrEmail]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifica]
                FROM [dbo].[TBLCLIENTES] WHERE IdCliente = @IdCliente";
                return conn.Query<Cliente>(query, new { IdCliente }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCliente"></param>
        /// <returns></returns>
        public int Delete(int IdCliente)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                return conn.Execute("DELETE FROM TBLCLIENTES WHERE IdCliente = @IdCliente", new { IdCliente });
            }
        }
    }
}
