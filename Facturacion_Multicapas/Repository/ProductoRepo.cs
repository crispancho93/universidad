﻿using Dapper;
using Models;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class ProductoRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        public int Save(Producto producto)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    producto.IdProducto,
                    producto.StrNombre,
                    producto.StrCodigo,
                    producto.NumPrecioCompra,
                    producto.NumPrecioVenta,
                    producto.IdCategoria,
                    producto.StrDetalle,
                    producto.StrFoto,
                    producto.NumStock,
                    producto.DtmFechaModifica,
                    producto.StrUsuarioModifica
                };
                return conn.Execute("actualizar_Producto", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Producto> GetAll()
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     p.[IdProducto]
                    ,p.[StrNombre]
                    ,p.[StrCodigo]
                    ,p.[NumPrecioCompra]
                    ,p.[NumPrecioVenta]
                    ,p.[IdCategoria]
                    ,c.[StrDescripcion] DescripcionCategoria
                    ,p.[StrDetalle]
                    ,p.[strFoto]
                    ,p.[NumStock]
                    ,p.[DtmFechaModifica]
                    ,p.[StrUsuarioModifica]
                FROM [dbo].[TBLPRODUCTO] p
                INNER JOIN [TBLCATEGORIA_PROD] c ON c.[IdCategoria] = p.[IdCategoria]";
                return conn.Query<Producto>(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdProducto"></param>
        /// <returns></returns>
        public Producto GetById(int IdProducto)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdProducto]
                    ,[StrNombre]
                    ,[StrCodigo]
                    ,[NumPrecioCompra]
                    ,[NumPrecioVenta]
                    ,[IdCategoria]
                    ,[StrDetalle]
                    ,[strFoto]
                    ,[NumStock]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifica]
                FROM [dbo].[TBLPRODUCTO] WHERE IdProducto = @IdProducto";
                return conn.Query<Producto>(query, new { IdProducto }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdProducto"></param>
        /// <returns></returns>
        public int Delete(int IdProducto)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                return conn.Execute("DELETE FROM TBLPRODUCTO WHERE IdProducto = @IdProducto", new { IdProducto });
            }
        }
    }
}
