﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Repository
{
    public class Statements
    {
        private static readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        public DataTable LoadTable(string table, string condition)
        {
            string Sql = "SELECT * FROM " + table + " " + condition;
            using (var conn = new SqlConnection(_strConn))
            {
                conn.Open();
                var da = new SqlDataAdapter(Sql, conn);
                var ds = new DataSet();
                da.Fill(ds, table);
                DataTable dt = new DataTable();
                dt = ds.Tables[table];
                return dt;
            }
        }

        public DataTable GetDataCmd(string cmd)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                conn.Open();
                var da = new SqlDataAdapter(cmd, conn);
                var dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
        }

        public int ExecuteSP(string query, Dictionary<string, dynamic> values)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                conn.Open();
                var cmd = new SqlCommand(query, conn)
                {
                    CommandType = CommandType.StoredProcedure
                };
                foreach (var value in values)
                {
                    cmd.Parameters.AddWithValue(value.Key, value.Value);
                }
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
