﻿using Dapper;
using Models;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Repository
{
    public  class CategoriaRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoria"></param>
        /// <returns></returns>
        public int Save(Categoria categoria)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var data = new
                {
                    categoria.IdCategoria,
                    categoria.StrDescripcion,
                    categoria.DtmFechaModifica,
                    categoria.StrUsuarioModifico
                };
                return conn.Execute("actualizar_CategoriaProd", data, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Categoria> GetAll()
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdCategoria]
                    ,[StrDescripcion]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifico]
                FROM [dbo].[TBLCATEGORIA_PROD]";
                return conn.Query<Categoria>(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public Categoria GetById(int IdCategoria)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = @"
                SELECT 
                     [IdCategoria]
                    ,[StrDescripcion]
                    ,[DtmFechaModifica]
                    ,[StrUsuarioModifico]
                FROM [dbo].[TBLCATEGORIA_PROD] WHERE IdCategoria = @IdCategoria";
                return conn.Query<Categoria>(query, new { IdCategoria }).FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public int Delete(int IdCategoria)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                return conn.Execute("DELETE FROM [TBLCATEGORIA_PROD] WHERE IdCategoria = @IdCategoria", new { IdCategoria });
            }
        }
    }
}
