﻿using Dapper;
using Models;
using System.Configuration;
using System.Data.SqlClient;

namespace Repository
{
    public class UserRepo
    {
        private readonly string _strConn = ConfigurationManager.AppSettings["strConn"];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Login(User user)
        {
            using (var conn = new SqlConnection(_strConn))
            {
                var query = "SELECT COUNT(*) FROM TBLSEGURIDAD WHERE StrUsuario = @StrUsuario AND StrClave = @StrClave";
                return conn.ExecuteScalar<bool>(query, new { user.StrUsuario, user.StrClave });
            }
        }
    }
}