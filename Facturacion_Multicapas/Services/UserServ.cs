﻿using Models;
using Repository;

namespace Services
{
    public class UserServ
    {
        private UserRepo _userRepository;

        public UserServ()
        {
            _userRepository = new UserRepo();
        }

        public bool Login(User user)
        {
            return _userRepository.Login(user);
        }
    }
}