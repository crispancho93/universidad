﻿using Models;
using Repository;
using System.Collections.Generic;

namespace Services
{
    public class RolServ
    {
        private readonly RolRepo _rolRepo;

        public RolServ()
        {
            _rolRepo = new RolRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rol"></param>
        /// <returns></returns>
        public int Save(Rol rol)
        {
            return _rolRepo.Save(rol);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Rol> GetAll()
        {
            return _rolRepo.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdRolEmpleado"></param>
        /// <returns></returns>
        public Rol GetById(int IdRolEmpleado)
        {
            return _rolRepo.GetById(IdRolEmpleado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdRolEmpleado"></param>
        /// <returns></returns>
        public int Delete(int IdRolEmpleado)
        {
            return _rolRepo.Delete(IdRolEmpleado);
        }
    }
}
