﻿using Repository;
using System.Data;

namespace Services
{
    public class FacturaServ
    {
        private readonly Statements _facturaServ;

        public FacturaServ()
        {
            _facturaServ = new Statements();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataTable GetAll()
        {
            var query = @"
            SELECT 
	             fc.[IdFactura]
                ,fc.[DtmFecha]
                ,fc.[IdCliente]
	            ,c.StrNombre StrNombreCliente
                ,fc.[IdEmpleado]
	            ,e.strNombre StrNombreEmpleado
                ,fc.[NumDescuento]
                ,fc.[NumImpuesto]
                ,fc.[NumValorTotal]
                ,fc.[IdEstado]
                ,fc.[DtmFechaModifica]
                ,fc.[StrUsuarioModifica]
            FROM [dbo].[TBLFACTURA] fc
            INNER JOIN [dbo].[TBLCLIENTES] c ON c.IdCliente = fc.IdCliente
            INNER JOIN [dbo].[TBLEMPLEADO] e ON e.IdEmpleado = fc.IdEmpleado";
            return _facturaServ.GetDataCmd(query);
        }
    }
}
