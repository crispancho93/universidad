﻿using Models;
using Repository;

namespace Services
{
    public class SeguridadServ
    {
        private readonly SeguridadRepo _seguridadRepo;

        public SeguridadServ()
        {
            _seguridadRepo = new SeguridadRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="security"></param>
        /// <returns></returns>
        public int Save(Seguridad security)
        {
            return _seguridadRepo.Save(security);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public Seguridad GetByEmploye(int IdEmpleado)
        {
            return _seguridadRepo.GetByEmploye(IdEmpleado);
        }
    }
}
