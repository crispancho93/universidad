﻿using Models;
using Repository;
using System.Collections.Generic;

namespace Services
{
    public class ClienteServ
    {
        private readonly ClienteRepo _clienteRepository;

        public ClienteServ()
        {
            _clienteRepository = new ClienteRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Save(Cliente user)
        {
            return _clienteRepository.Save(user);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Cliente> GetAll()
        {
            return _clienteRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCliente"></param>
        /// <returns></returns>
        public Cliente GetById(int IdCliente)
        {
            return _clienteRepository.GetById(IdCliente);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCliente"></param>
        /// <returns></returns>
        public int Delete(int IdCliente)
        {
            return _clienteRepository.Delete(IdCliente);
        }
    }
}
