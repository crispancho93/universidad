﻿using Models;
using Repository;
using System.Collections.Generic;

namespace Services
{
    public class EmpleadoServ
    {
        private readonly EmpleadoRepo _empleadoRepository;

        public EmpleadoServ()
        {
            _empleadoRepository = new EmpleadoRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Save(Empleado user)
        {
            return _empleadoRepository.Save(user);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Empleado> GetAll()
        {
            return _empleadoRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public Empleado GetById(int IdEmpleado)
        {
            return _empleadoRepository.GetById(IdEmpleado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdEmpleado"></param>
        /// <returns></returns>
        public int Delete(int IdEmpleado)
        {
            return _empleadoRepository.Delete(IdEmpleado);
        }
    }
}
