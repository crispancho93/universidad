﻿using Models;
using Repository;
using System.Collections.Generic;

namespace Services
{
    public class ProductoServ
    {
        private readonly ProductoRepo _productoRepository;

        public ProductoServ()
        {
            _productoRepository = new ProductoRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public int Save(Producto user)
        {
            return _productoRepository.Save(user);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Producto> GetAll()
        {
            return _productoRepository.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdProducto"></param>
        /// <returns></returns>
        public Producto GetById(int IdProducto)
        {
            return _productoRepository.GetById(IdProducto);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdProducto"></param>
        /// <returns></returns>
        public int Delete(int IdProducto)
        {
            return _productoRepository.Delete(IdProducto);
        }
    }
}
