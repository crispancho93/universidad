﻿using Models;
using Repository;
using System.Collections.Generic;

namespace Services
{
    public class CategoriaServ
    {
        private readonly CategoriaRepo _categoriaRepo;

        public CategoriaServ()
        {
            _categoriaRepo = new CategoriaRepo();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoria"></param>
        /// <returns></returns>
        public int Save(Categoria categoria)
        {
            return _categoriaRepo.Save(categoria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Categoria> GetAll()
        {
            return _categoriaRepo.GetAll();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public Categoria GetById(int IdCategoria)
        {
            return _categoriaRepo.GetById(IdCategoria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdCategoria"></param>
        /// <returns></returns>
        public int Delete(int IdCategoria)
        {
            return _categoriaRepo.Delete(IdCategoria);
        }
    }
}
