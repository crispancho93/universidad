﻿using System;
using System.Collections.Generic;

namespace facturacion.Models;

public partial class Tblrole
{
    public int IdRolEmpleado { get; set; }

    public string? StrDescripcion { get; set; }

    public virtual ICollection<Tblempleado> Tblempleados { get; } = new List<Tblempleado>();
}
