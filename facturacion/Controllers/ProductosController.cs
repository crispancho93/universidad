using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using facturacion.Models;

namespace facturacion.Controllers
{
    public class ProductosController : Controller
    {
        private readonly DbfacturasContext _context;

        public ProductosController(DbfacturasContext context)
        {
            _context = context;
        }

        // GET: Productos
        public async Task<IActionResult> Index()
        {
            var dbfacturasContext = _context.Tblproductos.Include(t => t.IdCategoriaNavigation);
            return View(await dbfacturasContext.ToListAsync());
        }

        // GET: Productos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblproducto = await _context.Tblproductos
                .Include(t => t.IdCategoriaNavigation)
                .FirstOrDefaultAsync(m => m.IdProducto == id);
            if (tblproducto == null)
            {
                return NotFound();
            }

            return View(tblproducto);
        }

        // GET: Productos/Create
        public IActionResult Create()
        {
            ViewData["IdCategoria"] = new SelectList(_context.TblcategoriaProds, "IdCategoria", "IdCategoria");
            return View();
        }

        // POST: Productos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdProducto,StrNombre,StrCodigo,NumPrecioCompra,NumPrecioVenta,IdCategoria,StrDetalle,StrFoto,NumStock,DtmFechaModifica,StrUsuarioModifica")] Tblproducto tblproducto)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblproducto);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCategoria"] = new SelectList(_context.TblcategoriaProds, "IdCategoria", "IdCategoria", tblproducto.IdCategoria);
            return View(tblproducto);
        }

        // GET: Productos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblproducto = await _context.Tblproductos.FindAsync(id);
            if (tblproducto == null)
            {
                return NotFound();
            }
            ViewData["IdCategoria"] = new SelectList(_context.TblcategoriaProds, "IdCategoria", "IdCategoria", tblproducto.IdCategoria);
            return View(tblproducto);
        }

        // POST: Productos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdProducto,StrNombre,StrCodigo,NumPrecioCompra,NumPrecioVenta,IdCategoria,StrDetalle,StrFoto,NumStock,DtmFechaModifica,StrUsuarioModifica")] Tblproducto tblproducto)
        {
            if (id != tblproducto.IdProducto)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblproducto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblproductoExists(tblproducto.IdProducto))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCategoria"] = new SelectList(_context.TblcategoriaProds, "IdCategoria", "IdCategoria", tblproducto.IdCategoria);
            return View(tblproducto);
        }

        // GET: Productos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblproducto = await _context.Tblproductos
                .Include(t => t.IdCategoriaNavigation)
                .FirstOrDefaultAsync(m => m.IdProducto == id);
            if (tblproducto == null)
            {
                return NotFound();
            }

            return View(tblproducto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblproducto = await _context.Tblproductos.FindAsync(id);
            _context.Tblproductos.Remove(tblproducto);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblproductoExists(int id)
        {
            return _context.Tblproductos.Any(e => e.IdProducto == id);
        }
    }
}
