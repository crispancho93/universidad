using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using facturacion.Models;

namespace facturacion.Controllers;

public class EmpleadosController : Controller
{
    private readonly DbfacturasContext _context;

    public EmpleadosController(DbfacturasContext context)
    {
        _context = context;
    }

    // GET: Empleados
    public async Task<IActionResult> Index()
    {
        return View(await _context.Tblclientes.ToListAsync());
    }

    // GET: Empleados/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var tblcliente = await _context.Tblclientes
            .FirstOrDefaultAsync(m => m.IdCliente == id);
        if (tblcliente == null)
        {
            return NotFound();
        }

        return View(tblcliente);
    }

    // GET: Empleados/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: Empleados/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("IdCliente,StrNombre,NumDocumento,StrDireccion,StrTelefono,StrEmail,DtmFechaModifica,StrUsuarioModifica")] Tblcliente tblcliente)
    {
        if (ModelState.IsValid)
        {
            _context.Add(tblcliente);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        return View(tblcliente);
    }

    // GET: Empleados/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var tblcliente = await _context.Tblclientes.FindAsync(id);
        if (tblcliente == null)
        {
            return NotFound();
        }
        return View(tblcliente);
    }

    // POST: Empleados/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("IdCliente,StrNombre,NumDocumento,StrDireccion,StrTelefono,StrEmail,DtmFechaModifica,StrUsuarioModifica")] Tblcliente tblcliente)
    {
        if (id != tblcliente.IdCliente)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(tblcliente);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblclienteExists(tblcliente.IdCliente))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }
        return View(tblcliente);
    }

    // GET: Empleados/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var tblcliente = await _context.Tblclientes
            .FirstOrDefaultAsync(m => m.IdCliente == id);
        if (tblcliente == null)
        {
            return NotFound();
        }

        return View(tblcliente);
    }

    // POST: Empleados/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var tblcliente = await _context.Tblclientes.FindAsync(id);
        _context.Tblclientes.Remove(tblcliente);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool TblclienteExists(int id)
    {
        return _context.Tblclientes.Any(e => e.IdCliente == id);
    }
}