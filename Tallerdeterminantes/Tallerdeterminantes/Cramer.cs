﻿namespace Tallerdeterminantes
{
    public class Cramer
    {
        private readonly int X = 0;
        private readonly int Y = 1;
        private readonly int Z = 2;
        private readonly int TI = 3;

        /// <summary>
        /// Data source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int DeterminanteX(int[,] source)
        {
            // TI, Y, Z
            int[,] matriz = LLenarMatriz(source, "X");
            int result = DeterminanteMatrizSarrus(matriz);
            return result;
        }

        /// <summary>
        /// Data source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int DeterminanteY(int[,] source)
        {
            // X, TI, Z
            int[,] matriz = LLenarMatriz(source, "Y");
            int result = DeterminanteMatrizSarrus(matriz);
            return result;

        }

        /// <summary>
        /// Data source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int DeterminanteZ(int[,] source)
        {
            // X, Y, Z
            int[,] matriz = LLenarMatriz(source, "Z");
            int result = DeterminanteMatrizSarrus(matriz);
            return result;

        }

        /// <summary>
        /// Data source
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int DeterminanteSystem(int[,] source)
        {
            // X, Y, Z
            int[,] matriz = LLenarMatriz(source, "SYSTEM");
            int result = DeterminanteMatrizSarrus(matriz);
            return result;

        }

        /// <summary>
        /// Multiplica la matriz en SARRUS una vez que se halla llenado
        /// </summary>
        /// <param name="matriz"></param>
        /// <returns></returns>
        private int DeterminanteMatrizSarrus(int[,] matriz)
        {
            int result = 0;

            int ppal1 = matriz[0, 0] * matriz[1, 1] * matriz[2, 2];
            int ppal2 = matriz[1, 0] * matriz[2, 1] * matriz[3, 2];
            int ppal3 = matriz[2, 0] * matriz[3, 1] * matriz[4, 2];

            int secundaria1 = matriz[0, 2] * matriz[1, 1] * matriz[2, 0];
            int secundaria2 = matriz[1, 2] * matriz[2, 1] * matriz[3, 0];
            int secundaria3 = matriz[2, 2] * matriz[3, 1] * matriz[4, 0];

            result = (ppal1 + ppal2 + ppal3) - (secundaria1 + secundaria2 + secundaria3);

            return result;
        }

        /// <summary>
        /// Carga la matriz para poder multiplicarla
        /// </summary>
        /// <param name="source"></param>
        /// <param name="variable"></param>
        /// <returns></returns>
        private int[,] LLenarMatriz(int[,] source, string variable)
        {
            // TI, Y, Z
            int[,] result = new int[5, 3];
            int count = 0;
            int uno = 0;
            int dos = 0;
            int tres = 0;

            switch (variable)
            {
                case "X":
                    // TI, Y, Z
                    uno = TI;
                    dos = Y;
                    tres = Z;
                    break;

                case "Y":
                    // X, TI, Z
                    uno = X;
                    dos = TI;
                    tres = Z;
                    break;

                case "Z":
                    // X, Y, Z
                    uno = X;
                    dos = Y;
                    tres = TI;
                    break;

                case "SYSTEM":
                    // X, Y, Z
                    uno = X;
                    dos = Y;
                    tres = Z;
                    break;
                default:
                    break;
            }


            for (int i = 0; i < result.GetLength(0); i++)
            {
                if (i > 2)
                {
                    result[i, 0] = source[count, uno];
                    result[i, 1] = source[count, dos];
                    result[i, 2] = source[count, tres];
                    count++;
                }
                else
                {
                    result[i, 0] = source[i, uno];
                    result[i, 1] = source[i, dos];
                    result[i, 2] = source[i, tres];
                }
            }

            return result;
        }
    }
}
