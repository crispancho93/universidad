﻿namespace Tallerdeterminantes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt00 = new System.Windows.Forms.NumericUpDown();
            this.txt10 = new System.Windows.Forms.NumericUpDown();
            this.txt20 = new System.Windows.Forms.NumericUpDown();
            this.txt21 = new System.Windows.Forms.NumericUpDown();
            this.txt11 = new System.Windows.Forms.NumericUpDown();
            this.txt01 = new System.Windows.Forms.NumericUpDown();
            this.txt22 = new System.Windows.Forms.NumericUpDown();
            this.txt12 = new System.Windows.Forms.NumericUpDown();
            this.txt02 = new System.Windows.Forms.NumericUpDown();
            this.txt23 = new System.Windows.Forms.NumericUpDown();
            this.txt13 = new System.Windows.Forms.NumericUpDown();
            this.txt03 = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lbX = new System.Windows.Forms.Label();
            this.lbZ = new System.Windows.Forms.Label();
            this.lbY = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txt00)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt01)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt02)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt03)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(193, 98);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(314, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(427, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Z";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(539, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "TI";
            // 
            // txt00
            // 
            this.txt00.Location = new System.Drawing.Point(196, 139);
            this.txt00.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt00.Name = "txt00";
            this.txt00.Size = new System.Drawing.Size(53, 20);
            this.txt00.TabIndex = 6;
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(196, 186);
            this.txt10.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(53, 20);
            this.txt10.TabIndex = 7;
            // 
            // txt20
            // 
            this.txt20.Location = new System.Drawing.Point(196, 240);
            this.txt20.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt20.Name = "txt20";
            this.txt20.Size = new System.Drawing.Size(53, 20);
            this.txt20.TabIndex = 8;
            // 
            // txt21
            // 
            this.txt21.Location = new System.Drawing.Point(317, 240);
            this.txt21.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt21.Name = "txt21";
            this.txt21.Size = new System.Drawing.Size(53, 20);
            this.txt21.TabIndex = 11;
            // 
            // txt11
            // 
            this.txt11.Location = new System.Drawing.Point(317, 186);
            this.txt11.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt11.Name = "txt11";
            this.txt11.Size = new System.Drawing.Size(53, 20);
            this.txt11.TabIndex = 10;
            // 
            // txt01
            // 
            this.txt01.Location = new System.Drawing.Point(317, 139);
            this.txt01.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt01.Name = "txt01";
            this.txt01.Size = new System.Drawing.Size(53, 20);
            this.txt01.TabIndex = 9;
            // 
            // txt22
            // 
            this.txt22.Location = new System.Drawing.Point(430, 240);
            this.txt22.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt22.Name = "txt22";
            this.txt22.Size = new System.Drawing.Size(53, 20);
            this.txt22.TabIndex = 14;
            // 
            // txt12
            // 
            this.txt12.Location = new System.Drawing.Point(430, 186);
            this.txt12.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt12.Name = "txt12";
            this.txt12.Size = new System.Drawing.Size(53, 20);
            this.txt12.TabIndex = 13;
            // 
            // txt02
            // 
            this.txt02.Location = new System.Drawing.Point(430, 139);
            this.txt02.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt02.Name = "txt02";
            this.txt02.Size = new System.Drawing.Size(53, 20);
            this.txt02.TabIndex = 12;
            // 
            // txt23
            // 
            this.txt23.Location = new System.Drawing.Point(542, 240);
            this.txt23.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt23.Name = "txt23";
            this.txt23.Size = new System.Drawing.Size(53, 20);
            this.txt23.TabIndex = 17;
            // 
            // txt13
            // 
            this.txt13.Location = new System.Drawing.Point(542, 186);
            this.txt13.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt13.Name = "txt13";
            this.txt13.Size = new System.Drawing.Size(53, 20);
            this.txt13.TabIndex = 16;
            // 
            // txt03
            // 
            this.txt03.Location = new System.Drawing.Point(542, 139);
            this.txt03.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.txt03.Name = "txt03";
            this.txt03.Size = new System.Drawing.Size(53, 20);
            this.txt03.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(286, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 20);
            this.label5.TabIndex = 18;
            this.label5.Text = "ECUACION 3X3 CRAMER";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.Location = new System.Drawing.Point(355, 383);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(87, 32);
            this.btnCalcular.TabIndex = 19;
            this.btnCalcular.Text = "CALCULAR";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // lbX
            // 
            this.lbX.AutoSize = true;
            this.lbX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbX.Location = new System.Drawing.Point(196, 287);
            this.lbX.Name = "lbX";
            this.lbX.Size = new System.Drawing.Size(0, 13);
            this.lbX.TabIndex = 20;
            // 
            // lbZ
            // 
            this.lbZ.AutoSize = true;
            this.lbZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbZ.Location = new System.Drawing.Point(427, 287);
            this.lbZ.Name = "lbZ";
            this.lbZ.Size = new System.Drawing.Size(0, 13);
            this.lbZ.TabIndex = 21;
            // 
            // lbY
            // 
            this.lbY.AutoSize = true;
            this.lbY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbY.Location = new System.Drawing.Point(314, 287);
            this.lbY.Name = "lbY";
            this.lbY.Size = new System.Drawing.Size(0, 13);
            this.lbY.TabIndex = 22;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbY);
            this.Controls.Add(this.lbZ);
            this.Controls.Add(this.lbX);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txt23);
            this.Controls.Add(this.txt13);
            this.Controls.Add(this.txt03);
            this.Controls.Add(this.txt22);
            this.Controls.Add(this.txt12);
            this.Controls.Add(this.txt02);
            this.Controls.Add(this.txt21);
            this.Controls.Add(this.txt11);
            this.Controls.Add(this.txt01);
            this.Controls.Add(this.txt20);
            this.Controls.Add(this.txt10);
            this.Controls.Add(this.txt00);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(816, 489);
            this.MinimumSize = new System.Drawing.Size(816, 489);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txt00)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt01)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt02)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt03)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown txt00;
        private System.Windows.Forms.NumericUpDown txt10;
        private System.Windows.Forms.NumericUpDown txt20;
        private System.Windows.Forms.NumericUpDown txt21;
        private System.Windows.Forms.NumericUpDown txt11;
        private System.Windows.Forms.NumericUpDown txt01;
        private System.Windows.Forms.NumericUpDown txt22;
        private System.Windows.Forms.NumericUpDown txt12;
        private System.Windows.Forms.NumericUpDown txt02;
        private System.Windows.Forms.NumericUpDown txt23;
        private System.Windows.Forms.NumericUpDown txt13;
        private System.Windows.Forms.NumericUpDown txt03;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label lbX;
        private System.Windows.Forms.Label lbZ;
        private System.Windows.Forms.Label lbY;
    }
}

