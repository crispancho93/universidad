﻿using System;
using System.Windows.Forms;

namespace Tallerdeterminantes
{
    public partial class Form1 : Form
    {
        private readonly Cramer _cramer;

        public Form1()
        {
            InitializeComponent();
            _cramer = new Cramer();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Visible = false;
            //ShowInTaskbar = false;
            //Opacity = 0;

            txt00.Value =  2;
            txt01.Value =  3;
            txt02.Value =  4;
            txt03.Value =  20;
            txt10.Value =  3;
            txt11.Value = -5;
            txt12.Value = -1;
            txt13.Value = -10;
            txt20.Value = -1;
            txt21.Value =  2;
            txt22.Value = -3;
            txt23.Value = -6;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int _00 = int.Parse(txt00.Value.ToString());
            int _01 = int.Parse(txt01.Value.ToString());
            int _02 = int.Parse(txt02.Value.ToString());
            int _03 = int.Parse(txt03.Value.ToString());
            int _10 = int.Parse(txt10.Value.ToString());
            int _11 = int.Parse(txt11.Value.ToString());
            int _12 = int.Parse(txt12.Value.ToString());
            int _13 = int.Parse(txt13.Value.ToString());
            int _20 = int.Parse(txt20.Value.ToString());
            int _21 = int.Parse(txt21.Value.ToString());
            int _22 = int.Parse(txt22.Value.ToString());
            int _23 = int.Parse(txt23.Value.ToString());

            int[,] source = {
                //X   Y   Z   TI 
                {  _00,  _01,  _02,  _03 },
                {  _10,  _11,  _12,  _13 },
                {  _20,  _21,  _22,  _23 },
            };

            int system = _cramer.DeterminanteSystem(source);
            int x = _cramer.DeterminanteX(source) / system;
            int y = _cramer.DeterminanteY(source) / system;
            int z = _cramer.DeterminanteZ(source) / system;

            lbX.Text = $"R:// {x}";
            lbY.Text = $"R:// {y}";
            lbZ.Text = $"R:// {z}";
        }
    }
}
