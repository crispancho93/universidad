﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaClientes : Form
    {
        public FrmListaClientes()
        {
            InitializeComponent();
        }

        private void FrmListaClientes_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvClientes.Rows.Clear();
            try
            {
                var query = "SELECT * FROM TBLCLIENTES";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvClientes.Rows.Add(
                        row["IdCliente"],
                        row["StrNombre"],
                        row["NumDocumento"],
                        row["StrDireccion"],
                        row["StrTelefono"], 
                        row["StrEmail"],
                        row["DtmFechaModifica"],
                        row["StrUsuarioModifica"]
                    );
                }
            } 
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCliente = new FrmEditarCliente
            {
                FrmParent = this
            };
            frmCliente.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvClientes.CurrentRow.Index;

            if (dgvClientes.Columns[e.ColumnIndex].Name == "btnEditar")
            {

                var frmCliente = new FrmEditarCliente
                {
                    IdCliente = int.Parse(dgvClientes[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCliente.ShowDialog();
            } else if (dgvClientes.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    var values = new Dictionary<string, dynamic>()
                    {
                        { "@IdCliente", dgvClientes[0, index].Value.ToString() }
                    };
                    try
                    {
                        Statements.ExecuteSP("Eliminar_Cliente", values);
                        MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvClientes[0, index].Value.ToString()}");
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error - {ex.Message}");
                    }
                }
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvClientes.Rows.Clear();
                try
                {
                    var query = $"SELECT * FROM TBLCLIENTES WHERE StrNombre LIKE '%{txtBuscar.Text}%'";
                    var dt = Statements.GetDataCmd(query);
                    foreach (DataRow row in dt.Rows)
                    {
                        dgvClientes.Rows.Add(
                            row["IdCliente"],
                            row["StrNombre"],
                            row["NumDocumento"],
                            row["StrDireccion"],
                            row["StrTelefono"],
                            row["StrEmail"],
                            row["DtmFechaModifica"],
                            row["StrUsuarioModifica"]
                        );
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error - {ex.Message}");
            }
        }
    }
}
