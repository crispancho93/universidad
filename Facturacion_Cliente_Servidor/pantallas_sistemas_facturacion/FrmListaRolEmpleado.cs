﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaRolEmpleado : Form
    {
        public FrmListaRolEmpleado()
        {
            InitializeComponent();
        }

        private void FrmListaRolEmpleado_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvRolEmpleado.Rows.Clear();
            try
            {
                var query = "SELECT * FROM TBLROLES";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvRolEmpleado.Rows.Add(
                        row["IdRolEmpleado"],
                        row["StrDescripcion"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmRolEmpleado = new FrmRolEmpleado
            {
                FrmParent = this
            };
            frmRolEmpleado.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvRolEmpleado.CurrentRow.Index;

            if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmRolEmpleado = new FrmRolEmpleado
                {
                    IdRolEmpleado = int.Parse(dgvRolEmpleado[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmRolEmpleado.ShowDialog();
            } else if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    var values = new Dictionary<string, dynamic>()
                    {
                        { "@IdRolEmpleado", dgvRolEmpleado[0, index].Value.ToString() }
                    };
                    try
                    {
                        Statements.ExecuteSP("Eliminar_RolEmpleado", values);
                        MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvRolEmpleado[0, index].Value.ToString()}");
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error - {ex.Message}");
                    }
                }
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvRolEmpleado.Rows.Clear();
                try
                {
                    var query = $"SELECT * FROM [dbo].[TBLROLES] WHERE [StrDescripcion] LIKE '%{txtBuscar.Text}%'";
                    var dt = Statements.GetDataCmd(query);
                    foreach (DataRow row in dt.Rows)
                    {
                        dgvRolEmpleado.Rows.Add(
                            row["IdRolEmpleado"],
                            row["StrDescripcion"]
                        );
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error - {ex.Message}");
            }
        }
    }
}
