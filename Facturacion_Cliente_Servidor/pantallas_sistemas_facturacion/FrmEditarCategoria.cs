﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarCategoria : Form
    {
        public int IdCategoria { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmEditarCategoria()
        {
            InitializeComponent();
        }

        private void FrmEditarCategoria_Load(object sender, EventArgs e)
        {
            if (IdCategoria == 0)
            {
                lbTitulo.Text = "NUEVO CATEGORIA";
            }
            else
            {
                var data = Statements.LoadTable("TBLCATEGORIA_PROD", $"WHERE IdCategoria = {IdCategoria}");
                lbTitulo.Text = "EDITAR CATEGORIA";
                txtNombre.Text = data.Rows[0]["StrDescripcion"].ToString();
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtNombre.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdCategoria", IdCategoria },
                    { "@Strdescripcion", txtNombre.Text },
                    { "@StrUsuarioModifico", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_CategoriaProducto", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
