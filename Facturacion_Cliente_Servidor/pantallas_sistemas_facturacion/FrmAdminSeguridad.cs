﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmAdminSeguridad : Form
    {
        public FrmAdminSeguridad()
        {
            InitializeComponent();
        }

        private void FrmAdminSeguridad_Load(object sender, EventArgs e)
        {
            var empleados = Statements.LoadTable("TBLEMPLEADO", "");
            cmbEmpleado.DataSource = empleados;
            cmbEmpleado.DisplayMember = "strNombre";
            cmbEmpleado.ValueMember = "IdEmpleado";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUsuario.Text) && !string.IsNullOrEmpty(txtClave.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdEmpleado", cmbEmpleado.SelectedValue },
                    { "@StrUsuario", txtUsuario.Text },
                    { "@StrClave", txtClave.Text },
                    { "@StrUsuarioModifico", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_Seguridad", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
