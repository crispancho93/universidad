﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarProducto : Form
    {
        public int IdProducto { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmEditarProducto()
        {
            InitializeComponent();
        }

        private void FrmEditarProducto_Load(object sender, EventArgs e)
        {
            var categorias = Statements.LoadTable("TBLCATEGORIA_PROD", "");
            cmbCategoria.DataSource = categorias;
            cmbCategoria.DisplayMember = "StrDescripcion";
            cmbCategoria.ValueMember = "IdCategoria";

            if (IdProducto == 0)
            {
                lbTitulo.Text = "NUEVO PRODUCTO";
            }
            else
            {
                var data = Statements.LoadTable("TBLPRODUCTO", $"WHERE IdProducto = {IdProducto}");

                lbTitulo.Text = "EDITAR PRODUCTO";
                txtNombre.Text = data.Rows[0]["StrNombre"].ToString();
                txtReferencia.Text = data.Rows[0]["StrCodigo"].ToString();
                txtPCompra.Text = data.Rows[0]["NumPrecioCompra"].ToString();
                txtPVenta.Text = data.Rows[0]["NumPrecioVenta"].ToString();
                txtStock.Text = data.Rows[0]["NumStock"].ToString();
                txtImagen.Text = data.Rows[0]["strFoto"].ToString();
                txtDetalles.Text = data.Rows[0]["StrDetalle"].ToString();

                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtReferencia.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdProducto", IdProducto },
                    { "@IdCategoria", cmbCategoria.SelectedValue },
                    { "@StrNombre", txtNombre.Text },
                    { "@StrCodigo", txtReferencia.Text },
                    { "@NumPrecioCompra", txtPCompra.Text },
                    { "@NumPrecioVenta", txtPVenta.Text },
                    { "@StrDetalle", txtDetalles.Text },
                    { "@strFoto", "Cristian" },
                    { "@NumStock", txtStock.Text },
                    { "@StrUsuarioModifica", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_Producto", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
