﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmRolEmpleado : Form
    {
        public int IdRolEmpleado { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmRolEmpleado()
        {
            InitializeComponent();
        }

        private void FrmRolEmpleado_Load(object sender, EventArgs e)
        {
            if (IdRolEmpleado == 0)
            {
                lbTitulo.Text = "NUEVO ROL EMPLEADO";
            }
            else
            {
                var data = Statements.LoadTable("TBLROLES", $"WHERE IdRolEmpleado = {IdRolEmpleado}");
                lbTitulo.Text = "EDITAR ROL EMPLEADO";
                txtNombre.Text = data.Rows[0]["StrDescripcion"].ToString();
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNombre.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdRolEmpleado", IdRolEmpleado },
                    { "@StrDescripcion", txtNombre.Text },
                };
                var affected = Statements.ExecuteSP("actualizar_RolEmpleado", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
