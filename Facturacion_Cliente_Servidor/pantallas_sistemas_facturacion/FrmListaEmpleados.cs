﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaEmpleados : Form
    {
        public FrmListaEmpleados()
        {
            InitializeComponent();
        }

        private void FrmListaEmpleados_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvEmpleados.Rows.Clear();
            try
            {
                var query = @"
                SELECT e.[IdEmpleado]
                      ,e.[strNombre]
                      ,e.[NumDocumento]
                      ,e.[StrDireccion]
                      ,e.[StrTelefono]
                      ,e.[StrEmail]
                      ,r.StrDescripcion StrRolEmpleado
                      ,e.[DtmIngreso]
                      ,e.[DtmRetiro]
                      ,e.[strDatosAdicionales]
                      ,e.[DtmFechaModifica]
                      ,e.[StrUsuarioModifico]
                  FROM [dbo].[TBLEMPLEADO] e
                  INNER JOIN TBLROLES r ON R.IdRolEmpleado = e.IdRolEmpleado
                ";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvEmpleados.Rows.Add(
                        row["IdEmpleado"],
                        row["strNombre"],
                        row["NumDocumento"],
                        row["StrEmail"],
                        row["StrTelefono"],
                        row["StrDireccion"],
                        Convert.ToDateTime(row["DtmIngreso"]).ToString("yyyy-MM-dd"),
                        Convert.ToDateTime(row["DtmRetiro"]).ToString("yyyy-MM-dd"),
                        row["StrRolEmpleado"],
                        row["strDatosAdicionales"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FrmEditarEmpleado
            {
                FrmParent = this
            };
            frm.ShowDialog();
        }

        private void dgvEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvEmpleados.CurrentRow.Index;

            if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frm = new FrmEditarEmpleado
                {
                    FrmParent = this,
                    IdEmpleado = int.Parse(dgvEmpleados[0, index].Value.ToString())
                };
                frm.ShowDialog();
            } else if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    var values = new Dictionary<string, dynamic>()
                    {
                        { "@IdEmpleado", dgvEmpleados[0, index].Value.ToString() }
                    };
                    try
                    {
                        Statements.ExecuteSP("Eliminar_Empleado", values);
                        MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvEmpleados[0, index].Value.ToString()}");
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error - {ex.Message}");
                    }
                }
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            dgvEmpleados.Rows.Clear();
            try
            {
                var query = @"
                SELECT e.[IdEmpleado]
                      ,e.[strNombre]
                      ,e.[NumDocumento]
                      ,e.[StrDireccion]
                      ,e.[StrTelefono]
                      ,e.[StrEmail]
                      ,r.StrDescripcion StrRolEmpleado
                      ,e.[DtmIngreso]
                      ,e.[DtmRetiro]
                      ,e.[strDatosAdicionales]
                      ,e.[DtmFechaModifica]
                      ,e.[StrUsuarioModifico]
                FROM [dbo].[TBLEMPLEADO] e
                INNER JOIN TBLROLES r ON R.IdRolEmpleado = e.IdRolEmpleado
                WHERE e.[strNombre] LIKE '%@strNombre%'";
                query = query.Replace("@strNombre", txtBuscar.Text.Trim());
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvEmpleados.Rows.Add(
                        row["IdEmpleado"],
                        row["strNombre"],
                        row["NumDocumento"],
                        row["StrEmail"],
                        row["StrTelefono"],
                        row["StrDireccion"],
                        Convert.ToDateTime(row["DtmIngreso"]).ToString("yyyy-MM-dd"),
                        Convert.ToDateTime(row["DtmRetiro"]).ToString("yyyy-MM-dd"),
                        row["StrRolEmpleado"],
                        row["strDatosAdicionales"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }
    }
}
