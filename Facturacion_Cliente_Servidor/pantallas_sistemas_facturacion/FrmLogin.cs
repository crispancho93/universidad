﻿using System;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            txtUsuario.Text = "pepe";
            txtClave.Text = "123";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            var user = txtUsuario.Text;
            var password = txtClave.Text;
            if (user != string.Empty && password != string.Empty)
            {
                var result = Statements.GetDataCmd($"SELECT * FROM TBLSEGURIDAD WHERE StrUsuario = '{user}' AND StrClave = '{password}'");
                if (result.Rows.Count > 0) 
                {
                    MessageBox.Show($"Bienvenido {user.ToUpper()}");
                    var frmPpal = new FrmPrincipal();
                    Hide();
                    frmPpal.Show();
                }
                else
                {
                    MessageBox.Show("Usuario o clave incorrectos, intenta de nuevo.");
                }
            }
            else
            {
                MessageBox.Show("El usuario y la clave son obligatorios.");
            }
        }
    }
}
