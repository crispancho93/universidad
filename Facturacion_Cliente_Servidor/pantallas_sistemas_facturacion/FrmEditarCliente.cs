﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarCliente : Form
    {
        public int IdCliente { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmEditarCliente()
        {
            InitializeComponent();
        }

        private void FrmEditarCliente_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO CLIENTE";
            }
            else
            {
                var data = Statements.LoadTable("TBLCLIENTES", $"WHERE IdCliente = {IdCliente}");
                lbTitulo.Text = "EDITAR CLIENTE";
                txtNombre.Text = data.Rows[0]["StrNombre"].ToString();
                txtDocumento.Text = data.Rows[0]["NumDocumento"].ToString();
                txtDireccion.Text = data.Rows[0]["StrDireccion"].ToString();
                txtTelefono.Text = data.Rows[0]["StrTelefono"].ToString();
                txtEmail.Text = data.Rows[0]["StrEmail"].ToString();
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDocumento.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdCliente", IdCliente },
                    { "@StrNombre", txtNombre.Text },
                    { "@NumDocumento", txtDocumento.Text },
                    { "@StrDireccion", txtDireccion.Text },
                    { "@StrTelefono", txtTelefono.Text },
                    { "@StrEmail", txtEmail.Text },
                    { "@StrUsuarioModifica", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_Cliente", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
