﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarEmpleado : Form
    {
        public int IdEmpleado { get; set; }
        public dynamic FrmParent { get; set; }

        public FrmEditarEmpleado()
        {
            InitializeComponent();
        }

        private void FrmEditarEmpleado_Load(object sender, EventArgs e)
        {
            var roles = Statements.LoadTable("TBLROLES", "");
            cmbRol.DataSource = roles;
            cmbRol.DisplayMember = "StrDescripcion";
            cmbRol.ValueMember = "IdRolEmpleado";

            if (IdEmpleado == 0)
            {
                lbTitulo.Text = "NUEVO EMPLEADO";
            }
            else
            {
                var data = Statements.LoadTable("TBLEMPLEADO", $"WHERE IdEmpleado = {IdEmpleado}");
                lbTitulo.Text = "EDITAR EMPLEADO";
                txtNombre.Text = data.Rows[0]["StrNombre"].ToString();
                txtDocumento.Text = data.Rows[0]["NumDocumento"].ToString();
                txtDireccion.Text = data.Rows[0]["StrDireccion"].ToString();
                txtTelefono.Text = data.Rows[0]["StrTelefono"].ToString();
                txtEmail.Text = data.Rows[0]["StrEmail"].ToString();
                txtDatosAdicionales.Text = data.Rows[0]["strDatosAdicionales"].ToString();
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDocumento.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdEmpleado", IdEmpleado },
                    { "@StrNombre", txtNombre.Text },
                    { "@NumDocumento", txtDocumento.Text },
                    { "@StrDireccion", txtDireccion.Text },
                    { "@StrTelefono", txtTelefono.Text },
                    { "@StrEmail", txtEmail.Text },
                    { "@IdRolEmpleado", cmbRol.SelectedValue },
                    { "@DtmIngreso", Convert.ToDateTime(txtFechaIngreso.Text) },
                    { "@DtmRetiro", Convert.ToDateTime(txtFechaRetiro.Text) },
                    { "@strDatosAdicionales", txtDatosAdicionales.Text },
                    { "@StrUsuarioModifico", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_Empleado", values);
                if (affected > 0)
                    MessageBox.Show("Modificado correctamente.", "Información");
                else
                    MessageBox.Show("No se modificaton datos.", "Información");
                FrmParent.LoadData();
                Close();
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }
    }
}
