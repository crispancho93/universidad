﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaProductos : Form
    {
        public FrmListaProductos()
        {
            InitializeComponent();
        }

        private void FrmListaProductos_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvProductos.Rows.Clear();
            try
            {
                var query = @"
                SELECT p.IdProducto
                      ,p.[StrNombre]
                      ,p.[StrCodigo]
                      ,p.[NumPrecioCompra]
                      ,p.[NumPrecioVenta]
                      ,c.StrDescripcion StrCategoria
                      ,p.[StrDetalle]
                      ,p.[strFoto]
                      ,p.[NumStock]
                      ,p.[DtmFechaModifica]
                      ,p.[StrUsuarioModifica]
                FROM [dbo].[TBLPRODUCTO] p
                LEFT JOIN TBLCATEGORIA_PROD c ON c.IdCategoria = p.IdCategoria";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvProductos.Rows.Add(
                        row["IdProducto"],
                        row["StrNombre"],
                        row["StrCodigo"],
                        row["StrCategoria"],
                        row["NumPrecioCompra"],
                        row["NumPrecioVenta"],
                        row["NumStock"],
                        row["strFoto"],
                        row["StrDetalle"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmProducto = new FrmEditarProducto
            {
                FrmParent = this
            };
            frmProducto.ShowDialog();
        }

        private void dgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvProductos.CurrentRow.Index;

            if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmProducto = new FrmEditarProducto
                {
                    IdProducto = int.Parse(dgvProductos[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmProducto.ShowDialog();
            } else if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    var values = new Dictionary<string, dynamic>()
                    {
                        { "@IdProducto", dgvProductos[0, index].Value.ToString() }
                    };
                    try
                    {
                        Statements.ExecuteSP("Eliminar_Producto", values);
                        MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvProductos[0, index].Value.ToString()}");
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error - {ex.Message}");
                    }
                }
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvProductos.Rows.Clear();
                try
                {
                    var query = @"
                    SELECT p.IdProducto
                        ,p.[StrNombre]
                        ,p.[StrCodigo]
                        ,p.[NumPrecioCompra]
                        ,p.[NumPrecioVenta]
                        ,c.StrDescripcion StrCategoria
                        ,p.[StrDetalle]
                        ,p.[strFoto]
                        ,p.[NumStock]
                        ,p.[DtmFechaModifica]
                        ,p.[StrUsuarioModifica]
                    FROM [dbo].[TBLPRODUCTO] p
                    LEFT JOIN TBLCATEGORIA_PROD c ON c.IdCategoria = p.IdCategoria
                    WHERE p.[StrNombre] LIKE '%@StrNombre%'";
                    query = query.Replace("@StrNombre", txtBuscar.Text.Trim());
                    var dt = Statements.GetDataCmd(query);
                    foreach (DataRow row in dt.Rows)
                    {
                        dgvProductos.Rows.Add(
                            row["IdProducto"],
                            row["StrNombre"],
                            row["StrCodigo"],
                            row["StrCategoria"],
                            row["NumPrecioCompra"],
                            row["NumPrecioVenta"],
                            row["NumStock"],
                            row["strFoto"],
                            row["StrDetalle"]
                        );
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error - {ex.Message}");
            }
        }
    }
}
