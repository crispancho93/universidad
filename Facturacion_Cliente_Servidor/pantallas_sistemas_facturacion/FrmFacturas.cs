﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmFacturas : Form
    {
        public int idFactura { get; set; }
        public FrmFacturas()
        {
            InitializeComponent();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNumFactura.Text) && !string.IsNullOrEmpty(txtValorTotal.Text))
            {
                var values = new Dictionary<string, dynamic>()
                {
                    { "@IdFactura", idFactura },
                    { "@DtmFecha", DateTime.Now },
                    { "@IdCliente", cmbCliente.SelectedValue },
                    { "@IdEmpleado", 1 },
                    { "@NumDescuento", 0 },
                    { "@NumImpuesto", txtImpuesto.Text },
                    { "@NumValorTotal", txtValorTotal.Text },
                    { "@IdEstado", 1 },
                    { "@StrUsuarioModifica", "Cristian" },
                    { "@DtmFechaModifica", DateTime.Now },
                };
                var affected = Statements.ExecuteSP("actualizar_Factura", values);
                if (affected > 0)
                {
                    MessageBox.Show("Modificado correctamente.", "Información");
                    LoadData();
                }
                else
                {
                    MessageBox.Show("No se modificaton datos.", "Información");
                }  
            }
            else
            {
                MessageBox.Show("Faltan campos obligatorios.", "Información");
            }
        }

        public void LoadData()
        {
            var clientes = Statements.LoadTable("TBLCLIENTES", "");
            cmbCliente.DataSource = clientes;
            cmbCliente.DisplayMember = "StrNombre";
            cmbCliente.ValueMember = "IdCliente";

            dgvFacturas.Rows.Clear();
            try
            {
                var query = "SELECT * FROM TBLFACTURA";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvFacturas.Rows.Add(
                        row["IdFactura"],
                        row["IdCliente"],
                        row["IdEmpleado"],
                        row["DtmFecha"],
                        row["NumValorTotal"],
                        row["NumImpuesto"],
                        row["IdEstado"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void FrmFacturas_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
