﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaCategorias : Form
    {
        public FrmListaCategorias()
        {
            InitializeComponent();
        }

        private void FrmListaCategorias_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            dgvCategorias.Rows.Clear();
            try
            {
                var query = "SELECT * FROM TBLCATEGORIA_PROD";
                var dt = Statements.GetDataCmd(query);
                foreach (DataRow row in dt.Rows)
                {
                    dgvCategorias.Rows.Add(
                        row["IdCategoria"],
                        row["StrDescripcion"]
                    );
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error {ex.Message}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCategoria = new FrmEditarCategoria
            {
                FrmParent = this
            };
            frmCategoria.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvCategorias.CurrentRow.Index;

            if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCategoria = new FrmEditarCategoria
                {
                    IdCategoria = int.Parse(dgvCategorias[0, index].Value.ToString()),
                    FrmParent = this
                };
                frmCategoria.ShowDialog();
            } else if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    var values = new Dictionary<string, dynamic>()
                    {
                        { "@IdCategoria", dgvCategorias[0, index].Value.ToString() }
                    };
                    try
                    {
                        Statements.ExecuteSP("Eliminar_CategoriaProducto", values);
                        MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvCategorias[0, index].Value.ToString()}");
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Error - {ex.Message}");
                    }
                }
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvCategorias.Rows.Clear();
                try
                {
                    var query = $"SELECT * FROM [dbo].[TBLCATEGORIA_PROD] WHERE [StrDescripcion] LIKE '%{txtBuscar.Text}%'";
                    var dt = Statements.GetDataCmd(query);
                    foreach (DataRow row in dt.Rows)
                    {
                        dgvCategorias.Rows.Add(
                            row["IdCategoria"],
                            row["StrDescripcion"]
                        );
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error - {ex.Message}");
            }
        }
    }
}
