﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace xmlJson
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnXml_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.ReadXml($"{Directory.GetCurrentDirectory()}\\empleados.xml");
            DataTable dt = ds.Tables[0];
            dgvEmpleados.DataSource = dt;
            lbMsg.Text = "empleados.xml";
            MessageBox.Show("XML cargado con éxito.");
        }

        private void btnJson_Click(object sender, EventArgs e)
        {
            string jsonString = File.ReadAllText($"{Directory.GetCurrentDirectory()}\\empleados.json");
            DataTable dt = (DataTable)JsonConvert.DeserializeObject(jsonString, (typeof(DataTable)));
            dgvEmpleados.DataSource = dt;
            lbMsg.Text = "empleados.json";
            MessageBox.Show("JSON cargado con éxito.");
        }
    }
}
