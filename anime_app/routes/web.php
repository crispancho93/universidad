<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public Routes
Route::get('/', 'App\Http\Controllers\UsuarioController@index');
Route::post('/login', 'App\Http\Controllers\UsuarioController@store');

Route::get('/register', 'App\Http\Controllers\UsuarioController@register');
Route::post('/register', 'App\Http\Controllers\UsuarioController@saveRegister');


// Private Routes
Route::group(['middleware' => ['customAuth']], function () {

    Route::get('/home', function () {
        return View('home');
    });

    Route::get('/personajes', 'App\Http\Controllers\PersonajesController@index');
    Route::get('/personajes/all', 'App\Http\Controllers\PersonajesController@all');
    Route::post('/personajes/store', 'App\Http\Controllers\PersonajesController@store');
    Route::put('/personajes/update', 'App\Http\Controllers\PersonajesController@update');
    Route::get('/personajes/show/{id}', 'App\Http\Controllers\PersonajesController@show');

    Route::get('/temporadas', 'App\Http\Controllers\TemporadasController@index');
    Route::get('/temporadas/all', 'App\Http\Controllers\TemporadasController@all');
    Route::post('/temporadas/store', 'App\Http\Controllers\TemporadasController@store');
    Route::put('/temporadas/update', 'App\Http\Controllers\TemporadasController@update');
    Route::get('/temporadas/show/{id}', 'App\Http\Controllers\TemporadasController@show');
});
