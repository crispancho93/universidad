@extends('layout')

@section('content')
<h4>PERSONAJES</h4>

{{-- TABLA --}}
<div class="card">
    <div class="card-body table-responsive">
        <table id="tbl-personajes" class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Color pelo</th>
                    <th scope="col">Arma</th>
                    <th scope="col">Poder</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

{{-- MODAL --}}
<div id="md-personajes" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar personaje</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frm-personajes">
                    @csrf
                    <input type="hidden" id="id" value="0">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="email" class="form-control" id="nombre" aria-describedby="emailHelp" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="color-pelo">Color pelo</label>
                        <input type="email" class="form-control" id="color-pelo" aria-describedby="emailHelp" placeholder="Color pelo">
                    </div>
                    <div class="form-group">
                        <label for="arma">Arma</label>
                        <input type="email" class="form-control" id="arma" aria-describedby="emailHelp" placeholder="Arma">
                    </div>
                    <div class="form-group">
                        <label for="poder">Poder</label>
                        <input type="email" class="form-control" id="poder" aria-describedby="emailHelp" placeholder="Poder">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="upsert()">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    // HADLERS
    $("#md-personajes").on("hidden.bs.modal", function() {
        $('#frm-personajes').trigger("reset");
        $('#id').val('0');
    });

    // LIST DATA
    let tbl_personajes;
    $(document).ready(function() {
        tbl_personajes = $("#tbl-personajes").DataTable({

            dom: '<"float-left"B><"float-right"f>rt<"row footer-controls-table"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            scrollX: true,
            order: [
                [0, "desc"]
            ],
            columns: [{
                    data: "nombre"
                },
                {
                    data: "color_pelo"
                },
                {
                    data: "arma"
                },
                {
                    data: "poder"
                },
            ],
            language: {
                search: "Buscar:",
                emptyTable: "No hay eventos en el momento",
                lengthMenu: "Mostrar _MENU_ eventos",
                info: "Mostrando _START_ a _END_ de _TOTAL_ eventos",
                infoEmpty: "Mostrando eventos del 0 al 0 de un total de 0 eventos",
                paginate: {
                    first: "Primero",
                    last: "Último",
                    next: "Siguiente",
                    previous: "Anterior",
                },
            },
            buttons: [{
                titleAttr: 'Nuevo',
                text: '+',
                action: function() {
                    $('#md-personajes').modal('show');
                }
            }],
        });

        populate();

        // LIST DATA ON CLICK
        $('#tbl-personajes tbody').on('click', 'tr', function() {

            const data = tbl_personajes.row(this).data();

            fetch(`/personajes/show/${data.id}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                })
                .then((response) => response.json())
                .then((data) => {

                    $('#id').val(data.id);
                    $('#nombre').val(data.nombre);
                    $('#color-pelo').val(data.color_pelo);
                    $('#arma').val(data.arma);
                    $('#poder').val(data.poder);

                    $('#md-personajes').modal('show');
                })
                .catch((error) => {
                    console.log('Error:', error);
                });
        });
    });

    // POPULATE TABLE
    function populate() {
        fetch(`/personajes/all`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('input[name="_token"]').val()
                },
            })
            .then((response) => response.json())
            .then((data) => {
                tbl_personajes.clear();
                tbl_personajes.rows.add(data).draw();
            })
            .catch((error) => {
                console.log('Error:', error);
            });
    }

    // SAVE AND UPDATE
    function upsert() {

        if (!$('#nombre').val() || !$('#color-pelo').val() || !$('#arma').val() || !$('#poder').val()) {
            alert('Todos los valores son obligatorios');
            return;
        }

        const data = {
            id: $('#id').val(),
            nombre: $('#nombre').val(),
            color_pelo: $('#color-pelo').val(),
            arma: $('#arma').val(),
            poder: $('#poder').val()
        }

        console.log(JSON.stringify(data))

        if ($('#id').val() == '0') {

            fetch('/personajes/store', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                    body: JSON.stringify(data),
                })
                .then((response) => response.json())
                .then((data) => {

                    alert('Agregado correctamente');

                    $('#frm-personajes').trigger("reset");
                    $('#md-personajes').modal('hide');

                    populate();

                })
                .catch((error) => {
                    console.log('Error:', error);
                });

        } else {

            fetch('/personajes/update', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                    body: JSON.stringify(data),
                })
                .then((response) => response.json())
                .then((data) => {

                    alert('Editado correctamente');

                    $('#frm-personajes').trigger("reset");
                    $('#md-personajes').modal('hide');

                    $('#id').val('0');

                    populate();

                })
                .catch((error) => {
                    $('#id').val('0');
                    console.log('Error:', error);
                });
        }
    }
</script>
<style>
    .footer-controls-table {
        padding-top: 10px
    }
</style>
@endsection