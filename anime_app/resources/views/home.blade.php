@extends('layout')

@section('content')
    <h2>Anime Home page</h2>
    <p>
        Al estar dirigido a públicos diversos y abarcar un amplio rango de temas, puede ser complicado definirlo. Una de sus
        características principales es el desarrollo de tramas complejas a lo largo de muchos episodios. Así se diferenciaba
        de producciones occidentales, más orientadas al público infantil. Ha tratado temas como la relación con la
        naturaleza, el sentido colectivo del deber y hasta el existencialismo. Con un tono maduro, podían chocarnos algunas
        escenas de violencia o sexo.

        Otra faceta reconocible es la de los rasgos de sus personajes. Es reconocible el tamaño de los ojos, en contraste
        desproporcionado con la nariz y la boca. Y aunque hace tiempo que se estandarizó el uso de computadoras, su estilo
        suele recordar al dibujo a mano del manga.
    </p>
@endsection
