@extends('layout')

@section('content')
<h4>TEMPORADAS</h4>

{{-- TABLA --}}
<div class="card">
    <div class="card-body table-responsive">
        <table id="tbl-temporadas" class="table table-bordered table-hover table-striped" style="width: 100%">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Capítulos</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

{{-- MODAL --}}
<div id="md-temporadas" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar temporada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frm-temporadas">
                    @csrf
                    <input type="hidden" id="id" value="0">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="email" class="form-control" id="nombre" aria-describedby="emailHelp" placeholder="Nombre">
                    </div>
                    <div class="form-group">
                        <label for="capitulos">Capítulos</label>
                        <input type="email" class="form-control" id="capitulos" aria-describedby="emailHelp" placeholder="Capítulos">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="upsert()">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    // HADLERS
    $("#md-temporadas").on("hidden.bs.modal", function() {
        $('#frm-temporadas').trigger("reset");
        $('#id').val('0');
    });

    // LIST DATA
    let tbl_temporadas;
    $(document).ready(function() {
        tbl_temporadas = $("#tbl-temporadas").DataTable({

            dom: '<"float-left"B><"float-right"f>rt<"row footer-controls-table"<"col-sm-4"l><"col-sm-4"i><"col-sm-4"p>>',
            scrollX: true,
            order: [
                [0, "desc"]
            ],
            columns: [{
                    data: "nombre"
                },
                {
                    data: "capitulos"
                },
            ],
            language: {
                search: "Buscar:",
                emptyTable: "No hay eventos en el momento",
                lengthMenu: "Mostrar _MENU_ eventos",
                info: "Mostrando _START_ a _END_ de _TOTAL_ eventos",
                infoEmpty: "Mostrando eventos del 0 al 0 de un total de 0 eventos",
                paginate: {
                    first: "Primero",
                    last: "Último",
                    next: "Siguiente",
                    previous: "Anterior",
                },
            },
            buttons: [{
                titleAttr: 'Nuevo',
                text: '+',
                action: function() {
                    $('#md-temporadas').modal('show');
                }
            }],
        });

        populate();

        // LIST DATA ON CLICK
        $('#tbl-temporadas tbody').on('click', 'tr', function() {

            const data = tbl_temporadas.row(this).data();

            fetch(`/temporadas/show/${data.id}`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                })
                .then((response) => response.json())
                .then((data) => {

                    $('#id').val(data.id);
                    $('#nombre').val(data.nombre);
                    $('#capitulos').val(data.capitulos);

                    $('#md-temporadas').modal('show');
                })
                .catch((error) => {
                    console.log('Error:', error);
                });
        });
    });

    // POPULATE TABLE
    function populate() {
        fetch(`/temporadas/all`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('input[name="_token"]').val()
                },
            })
            .then((response) => response.json())
            .then((data) => {
                tbl_temporadas.clear();
                tbl_temporadas.rows.add(data).draw();
            })
            .catch((error) => {
                console.log('Error:', error);
            });
    }

    // SAVE AND UPDATE
    function upsert() {

        if (!$('#nombre').val() || !$('#capitulos').val()) {
            alert('Todos los valores son obligatorios');
            return;
        }

        const data = {
            id: $('#id').val(),
            nombre: $('#nombre').val(),
            capitulos: $('#capitulos').val(),
        }

        console.log(JSON.stringify(data))

        if ($('#id').val() == '0') {

            fetch('/temporadas/store', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                    body: JSON.stringify(data),
                })
                .then((response) => response.json())
                .then((data) => {
                    alert('Agregado correctamente');

                    $('#frm-temporadas').trigger("reset");
                    $('#md-temporadas').modal('hide');

                    populate();

                })
                .catch((error) => {
                    console.log('Error:', error);
                });

        } else {

            fetch('/temporadas/update', {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('input[name="_token"]').val()
                    },
                    body: JSON.stringify(data),
                })
                .then((response) => response.json())
                .then((data) => {

                    alert('Editado correctamente');

                    $('#frm-temporadas').trigger("reset");
                    $('#md-temporadas').modal('hide');

                    $('#id').val('0');

                    populate();

                })
                .catch((error) => {
                    $('#id').val('0');
                    console.log('Error:', error);
                });
        }
    }
</script>
<style>
    .footer-controls-table {
        padding-top: 10px
    }
</style>
@endsection