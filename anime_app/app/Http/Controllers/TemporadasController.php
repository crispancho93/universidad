<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Temporada;

class TemporadasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('temporadas');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = Temporada::all();
        return response($data, 200)->header('Content-Type', 'application/json');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $temporada = new Temporada;

            $temporada->nombre = $request->nombre;
            $temporada->capitulos = $request->capitulos;

            $temporada->save();

            return response(['msg' => 'ok'], 200)->header('Content-Type', 'application/json');
        } catch (\Throwable $th) {
            return response(['msg' => $th->getMessage()], 400)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            $temporada = Temporada::find($request->id);

            $temporada->nombre = $request->nombre;
            $temporada->capitulos = $request->capitulos;

            $temporada->update();

            return response(['msg' => 'ok'], 200)->header('Content-Type', 'application/json');
        } catch (\Throwable $th) {
            return response(['msg' => $th->getMessage()], 400)->header('Content-Type', 'application/json');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $temporada = Temporada::where('id', $id)->first();
        if ($temporada) {
            return response($temporada, 200)->header('Content-Type', 'application/json');
        } else {
            return response(['msg' => 'Not found'], 400)->header('Content-Type', 'application/json');
        }
    }
}
