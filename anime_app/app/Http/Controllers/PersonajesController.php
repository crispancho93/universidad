<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personaje;

class PersonajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View('personajes');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = Personaje::all();
        return response($data, 200)->header('Content-Type', 'application/json');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $personaje = new Personaje;

            $personaje->nombre = $request->nombre;
            $personaje->color_pelo = $request->color_pelo;
            $personaje->arma = $request->arma;
            $personaje->poder = $request->poder;

            $personaje->save();

            return response(['msg' => 'ok'], 200)->header('Content-Type', 'application/json');
        } catch (\Throwable $th) {
            return response(['msg' => $th->getMessage()], 400)->header('Content-Type', 'application/json');
        }
    }

    /**
     * Update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            $personaje = Personaje::find($request->id);

            $personaje->nombre = $request->nombre;
            $personaje->color_pelo = $request->color_pelo;
            $personaje->arma = $request->arma;
            $personaje->poder = $request->poder;

            $personaje->update();

            return response(['msg' => 'ok'], 200)->header('Content-Type', 'application/json');
        } catch (\Throwable $th) {
            return response(['msg' => $th->getMessage()], 400)->header('Content-Type', 'application/json');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $personaje = Personaje::where('id', $id)->first();
        if ($personaje) {
            return response($personaje, 200)->header('Content-Type', 'application/json');
        } else {
            return response(['msg' => 'Not found'], 400)->header('Content-Type', 'application/json');
        }
    }
}
