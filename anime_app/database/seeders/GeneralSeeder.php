<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personajes')->insert([
            'nombre' => "Ichigo Kurosaki",
            'color_pelo' => "Naranga",
            'arma' => "Espada",
            'poder' => "Huecoficacion",
        ]);

        DB::table('temporadas')->insert([
            'nombre' => "Shinigami Sustituto",
            'capitulos' => "20",
        ]);

        DB::table('users')->insert([
            'name' => "test",
            'email' => "test",
            'password' => "40bd001563085fc35165329ea1ff5c5ecbdbbeef",
        ]);
    }
}
