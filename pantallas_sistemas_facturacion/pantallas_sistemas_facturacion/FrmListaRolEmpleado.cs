﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaRolEmpleado : Form
    {
        public FrmListaRolEmpleado()
        {
            InitializeComponent();
        }

        private void FrmListaRolEmpleado_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            for(int i = 1; i < 10; i++)
            {
                dgvRolEmpleado.Rows.Add(i, $"Nombre ROL{i}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmRolEmpleado = new FrmRolEmpleado();
            frmRolEmpleado.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvRolEmpleado.CurrentRow.Index;

            if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmRolEmpleado = new FrmRolEmpleado();
                frmRolEmpleado.IdCliente = int.Parse(dgvRolEmpleado[0, index].Value.ToString());
                frmRolEmpleado.ShowDialog();
            } else if (dgvRolEmpleado.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvRolEmpleado[0, index].Value.ToString()}");
                }
            }

        }
    }
}
