﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaProductos : Form
    {
        public FrmListaProductos()
        {
            InitializeComponent();
        }

        private void FrmListaProductos_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            for(int i = 1; i < 10; i++)
            {
                dgvProductos.Rows.Add(i, $"Producto {i}", $"Referencia {i}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmProducto = new FrmEditarProducto();
            frmProducto.ShowDialog();
        }

        private void dgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvProductos.CurrentRow.Index;

            if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmProducto = new FrmEditarProducto();
                frmProducto.IdCliente = int.Parse(dgvProductos[0, index].Value.ToString());
                frmProducto.ShowDialog();
            } else if (dgvProductos.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvProductos[0, index].Value.ToString()}");
                }
            }

        }
    }
}
