﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaCategorias : Form
    {
        public FrmListaCategorias()
        {
            InitializeComponent();
        }

        private void FrmListaCategorias_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            for(int i = 1; i < 10; i++)
            {
                dgvCategorias.Rows.Add(i, $"Nombre cateroría{i}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCategoria = new FrmEditarCategoria();
            frmCategoria.ShowDialog();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvCategorias.CurrentRow.Index;

            if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCategoria = new FrmEditarCategoria();
                frmCategoria.IdCliente = int.Parse(dgvCategorias[0, index].Value.ToString());
                frmCategoria.ShowDialog();
            } else if (dgvCategorias.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvCategorias[0, index].Value.ToString()}");
                }
            }

        }
    }
}
