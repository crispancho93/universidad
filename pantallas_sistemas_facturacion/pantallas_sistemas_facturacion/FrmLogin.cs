﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmLogin : Form
    {
        private readonly string _user = "crispancho";
        private readonly string _password = "123";

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            txtUsuario.Text = "crispancho";
            txtClave.Text = "123";
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            var user = txtUsuario.Text;
            var password = txtClave.Text;
            if (user != string.Empty && password != string.Empty)
            {
                if (user == _user && password == _password)
                {
                    MessageBox.Show($"Bienvenido {_user.ToUpper()}");
                    var frmPpal = new FrmPrincipal();
                    Hide();
                    frmPpal.Show();
                }
                else
                {
                    MessageBox.Show("Usuario o clave incorrectos, intenta de nuevo.");
                }
            }
            else
            {
                MessageBox.Show("El usuario y la clave son obligatorios.");
            }
        }
    }
}
