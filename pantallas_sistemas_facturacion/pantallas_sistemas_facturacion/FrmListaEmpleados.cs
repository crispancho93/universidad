﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmListaEmpleados : Form
    {
        public FrmListaEmpleados()
        {
            InitializeComponent();
        }

        private void FrmListaEmpleados_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void LoadData()
        {
            for(int i = 1; i < 10; i++)
            {
                dgvEmpleados.Rows.Add(i, $"Nombre Empleado {i} Apellido 1 Apellidos 2", $"Cedula {i}", $"Correo {i}");
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frmCliente = new FrmEditarEmpleado();
            frmCliente.ShowDialog();
        }

        private void dgvEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var index = dgvEmpleados.CurrentRow.Index;

            if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEditar")
            {
                var frmCliente = new FrmEditarEmpleado();
                frmCliente.IdCliente = int.Parse(dgvEmpleados[0, index].Value.ToString());
                frmCliente.ShowDialog();
            } else if (dgvEmpleados.Columns[e.ColumnIndex].Name == "btnEliminar")
            {
                var confirmResult = MessageBox.Show("¿Seguro que desea continuar, esta acción no se puede revertir?",
                                     "CONFIRMAR!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    MessageBox.Show($"Borrado correctamente indice {e.RowIndex} ID {dgvEmpleados[0, index].Value.ToString()}");
                }
            }

        }
    }
}
