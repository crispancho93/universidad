﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmRolEmpleado : Form
    {
        public int IdCliente { get; set; }

        public FrmRolEmpleado()
        {
            InitializeComponent();
        }

        private void FrmRolEmpleado_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO ROL EMPLEADO";
            }
            else
            {
                lbTitulo.Text = "EDITAR ROL EMPLEADO";
                txtNombre.Text = "ADMIN";
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
