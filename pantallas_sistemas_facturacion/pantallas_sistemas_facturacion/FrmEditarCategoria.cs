﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarCategoria : Form
    {
        public int IdCliente { get; set; }

        public FrmEditarCategoria()
        {
            InitializeComponent();
        }

        private void FrmEditarCategoria_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO CATEGORIA";
            }
            else
            {
                lbTitulo.Text = "EDITAR CATEGORIA";
                txtNombre.Text = "Pantaloneta";
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
