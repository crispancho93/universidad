﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarCliente : Form
    {
        public int IdCliente { get; set; }

        public FrmEditarCliente()
        {
            InitializeComponent();
        }

        private void FrmEditarCliente_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO CLIENTE";
            }
            else
            {
                lbTitulo.Text = "EDITAR CLIENTE";
                txtNombre.Text = "Pepito Perez";
                txtDocumento.Text = "1234567";
                txtDireccion.Text = "Calle siempre buena 123 springfield";
                txtTelefono.Text = "7777777";
                txtEmail.Text = "peipto@gmail.com";
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
