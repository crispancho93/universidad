﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pantallas_sistemas_facturacion
{
    public partial class FrmEditarProducto : Form
    {
        public int IdCliente { get; set; }

        public FrmEditarProducto()
        {
            InitializeComponent();
        }

        private void FrmEditarProducto_Load(object sender, EventArgs e)
        {
            if (IdCliente == 0)
            {
                lbTitulo.Text = "NUEVO PRODUCTO";
            }
            else
            {
                lbTitulo.Text = "EDITAR PRODUCTO";
                txtNombre.Text = "Pantaloneta Nike M";
                txtReferencia.Text = "REF-0001";
                btnAgregar.Text = "EDITAR";
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
